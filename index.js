module.exports = {
  uses: ['view', 'common', 'intents'],
  init: function(view, common, intents, options) {
    var $carousel,
      self = this;

    this.intents = intents;
    // @TODO abstract
    $('head').append($('<link />', {
      rel: 'stylesheet',
      href: __dirname + '/stylesheet.css',
      type: 'text/css'
    }));

    // @TODO pull from config
    view('index', {
      items: [{
        label: 'Movies',
        background: 'assets/movies.jpg',
        intent: 'nwmc.module.launch',
        params: {
          module: 'mod_movie_browser'
        }
      }, {
        label: 'Games',
        background: 'assets/games.jpg',
        intent: 'ACTIVITY_GAMES',
        params: {

          // @TODO path from config
          path: '/home/james/media/Games'
        }
      }, {
        label: 'Music',
        background: 'assets/music.jpg',
        intent: 'nwmc.module.launch',
        params: {
          module: 'mod_music_browser'
        }
      }, {
        label: 'TV Shows',
        background: 'assets/tvshows.jpg',
        intent: 'nwmc.module.launch',
        params: {
          module: 'mod_tvshows_browser'
        }
      }, {
        label: 'Settings',
        background: 'assets/settings.jpg',
        intent: 'nwmc.module.launch',
        params: {
          module: 'mod_settings'
        }
      }, {
        label: 'Exit',
        background: 'assets/exit.jpg',
        intent: 'nwmc.system.exit'
      }]
    }, this.element);

    $carousel = this.element.find('.carousel');

    setTimeout(function() {
      $carousel.find('.item.active a').focus();
    }, 10);
  },
  events: {
    '.carousel slide.bs.carousel': function(el, ev) {
      el.find('.item .item-bg').removeClass('in');
    },
    '.carousel slid.bs.carousel': function(el, ev) {
      el.find('.item .item-bg').addClass('in');
      el.find('.item.active a').focus();
    },
    '.carousel-caption a click': function(el, ev) {
      this.intents.execute(el.data('intent'));
    },
    'input.left': function() {
      this.element.find('.carousel').carousel('prev');
    },
    'input.right': function() {
      this.element.find('.carousel').carousel('next');
    },
    'input.select': function() {
      this.element.find('.carousel').find('.item.active a').trigger('click');
    }
  },
  intents: {
    filters: ['ACTIVITY_MAIN_MENU']
  }
};
